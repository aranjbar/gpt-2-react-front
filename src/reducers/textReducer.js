import {
  PROCESS_TEXT,
  DETECT_LANGUAGE,
  TRANSLATE_TEXT
} from '../actions/types';

const initialState = {
  input: '',
  output: '',
  sourceLanguage: ''
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PROCESS_TEXT:
      return {
        ...state,
        output: action.payload.samples
      };
    case DETECT_LANGUAGE:
      return {
        ...state,
        sourceLanguage: action.payload.lang || ''
      };
    case TRANSLATE_TEXT:
      return {
        ...state,
        output: action.payload.text[0]
      };
    default:
      return state;
  }
}
