import { PROCESS_TEXT, DETECT_LANGUAGE, TRANSLATE_TEXT } from './types';
import axios from 'axios';

export const processText = text => async dispatch => {
  const res = await axios.post(`${process.env.REACT_APP_API_URL}/sample`, text);
  console.log(res.data);
  dispatch({
    type: PROCESS_TEXT,
    payload: res.data
  });
};

export const detectLanguage = text => async dispatch => {
  const res = await axios.post(`${process.env.REACT_APP_API_URL}/detect`, text);
  dispatch({
    type: DETECT_LANGUAGE,
    payload: JSON.parse(res.data)
  });
};

export const translateText = text => async dispatch => {
  const res = await axios.post(
    `${process.env.REACT_APP_API_URL}/translate`,
    text
  );
  console.log(res.data);
  dispatch({
    type: TRANSLATE_TEXT,
    payload: JSON.parse(res.data)
  });
};
