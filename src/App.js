import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/layout/Header';
import TlidBar from './components/layout/TlidBar';
import HomePage from './components/home-page/HomePage';

import { Provider } from 'react-redux';
import store from './store';

import './assets/fonts/iransans-fonts/fonts.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.scss';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Header />
            <TlidBar />
            <div className="container">
              <Switch>
                <Route exact path="/" component={HomePage} />
                {/* <Route exact path="/about" component={About} />
                <Route component={NotFound} /> */}
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
