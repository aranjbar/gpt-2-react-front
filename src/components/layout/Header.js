import React from 'react';
import { Link } from 'react-router-dom';
import favicon from '../../assets/favicon.png';

import '../../styles/Header.scss';

const Header = props => {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-expand-sm navbar-light bg-light"
      dir="rtl"
    >
      <div className="container">
        <a className="nav-link" href="/">
          <div className="navbar-brand-image">
            <img src={favicon} alt="GPT-2" />
          </div>
        </a>

        <div>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link to="/about" className="nav-link">
                درباره‌ی ما
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
