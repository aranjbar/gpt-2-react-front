import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  processText,
  detectLanguage,
  translateText
} from '../../actions/textActions';
import { toFullName } from '../../helpers/langFullName';
import classnames from 'classnames';

import './HomePage.scss';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
  }

  state = {
    inputText: '',
    sl: '',
    tl: 'en',
    detecting: false,
    detected: false,
    result: '',
    processing: false,
    translating: false
  };

  submit = async e => {
    const { inputText } = this.state;

    if (inputText === '') {
      return;
    }

    this.setState({ processing: true });
    const text = {
      text: inputText
    };
    await this.props.processText(text);
    this.setState({
      result: this.props.result,
      processed: true,
      processing: false
    });
  };

  translate = async e => {
    const newTl = e.target.name;
    const { result, tl } = this.state;
    if (result === '' || tl === newTl) {
      return;
    }
    const text = {
      sl: tl,
      tl: newTl,
      text: result
    };
    this.setState({ translating: true });
    await this.props.translateText(text);
    this.setState({ result: this.props.result, translating: false, tl: newTl });
  };

  onChange = async e => {
    const inputText = e.target.value;
    clearTimeout(this.timer);
    const timer = setTimeout(async () => {
      if (inputText === '') {
        return;
      }
      this.setState({ detecting: true });
      const text = {
        text: inputText
      };
      await this.props.detectLanguage(text);
      this.setState({ sl: this.props.sl, detected: true, detecting: false });
    }, 500);
    this.timer = timer;

    if (inputText === '') {
      this.setState({ sl: '', detected: false, detecting: false, result: '' });
    }
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    console.log(this.state);
    const {
      detected,
      detecting,
      sl,
      tl,
      inputText,
      processing,
      result,
      translating
    } = this.state;
    return (
      <div className="container">
        <div className="page-content-wrap">
          <div className="tlid-language-bar">
            <div className="sl-selector">
              <span dir="rtl">
                {detecting && inputText !== ''
                  ? 'شناسایی زبان...'
                  : detected
                  ? sl !== ''
                    ? `شناسایی شد - ${toFullName(sl)}`
                    : 'شناسایی نشد'
                  : 'شناسایی زبان'}
              </span>
            </div>
            <div className="process-button">
              <i
                className="fas fa-arrow-circle-right fa-lg"
                onClick={this.submit}
              />
            </div>
            <div className="tl-selector">
              <button
                id={classnames({ selected: tl === 'fa' })}
                name="fa"
                onClick={this.translate}
              >
                Persian
              </button>
              <button
                id={classnames({ selected: tl === 'en' })}
                name="en"
                onClick={this.translate}
              >
                English
              </button>
            </div>
          </div>
          <div className="source-result-wrap">
            <div className="source-wrap">
              <textarea
                placeholder={classnames({
                  rtl: sl === 'متن را وارد کنید',
                  ltr: sl === 'Input text'
                })}
                name="inputText"
                onChange={this.onChange}
                dir={classnames({ rtl: sl === 'fa', ltr: sl === 'en' })}
              />
            </div>
            <div className="result-wrap">
              {processing ? (
                <p dir="rtl" id="generating">
                  در حال پردازش...
                </p>
              ) : translating ? (
                <p dir="rtl" id="generating">
                  در حال ترجمه...
                </p>
              ) : (
                <p
                  dir={classnames({ rtl: tl === 'fa', ltr: tl === 'en' })}
                  className={classnames({ fa: tl === 'fa' })}
                >
                  {result}
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  sl: state.text.sourceLanguage,
  result: state.text.output
});

export default connect(
  mapStateToProps,
  { processText, detectLanguage, translateText }
)(HomePage);
