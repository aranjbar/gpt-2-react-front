export const toFullName = lang => {
  switch (lang.toLowerCase()) {
    case 'en':
      return 'انگلیسی';
    case 'fa':
      return 'فارسی';
    default:
      return '';
  }
};
